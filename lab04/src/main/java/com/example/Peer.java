package com.example;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Peer {
  private final int serverPort;
  private final int clientPort;
  private final String name;

  public Peer(int serverPort, int clientPort, String name) {
    this.clientPort = clientPort;
    this.serverPort = serverPort;
    this.name = name;
  }

  public void start() {
    new Thread(this::startServer).start();
    startClient();
  }

  private void startServer() {
    try (ServerSocket serverSocket = new ServerSocket(serverPort)) {
      System.out.println("Server started on port " + serverPort);
      System.out.println("Enter message: ");
      while (true) {
        Socket clientSocket = serverSocket.accept();
        new Thread(() -> handleClient(clientSocket)).start();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void startClient() {
    try (Scanner scanner = new Scanner(System.in)) {
      while (true) {
        System.out.println("Enter message: ");
        String message = scanner.nextLine();

        if (message.equalsIgnoreCase("exit")) {
          break;
        }


        String ipAddress = "localhost";
        int peerPort = clientPort;

        sendMessage(ipAddress, peerPort, message);
      }
    }
  }

  private void handleClient(Socket socket) {
    try (Scanner scanner = new Scanner(socket.getInputStream());
        PrintWriter writer = new PrintWriter(socket.getOutputStream(), true)) {

      while (true) {
        if (scanner.hasNextLine()) {
          String receivedMessage = scanner.nextLine();
          System.out.println("Received from " + socket.getInetAddress() + ": " + receivedMessage);
          System.out.println("Enter message: ");
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void sendMessage(String ipAddress, int peerPort, String message) {
    try (Socket socket = new Socket(ipAddress, peerPort);
        PrintWriter writer = new PrintWriter(socket.getOutputStream(), true)) {

      writer.println(name + ": " + message);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    try (Scanner scanner = new Scanner(System.in)) {
      System.out.print("Enter your name: ");
      String name = scanner.nextLine();

      System.out.print("Enter client port: ");
      int clientPort = Integer.parseInt(scanner.nextLine());

      System.out.print("Enter server port: ");
      int serverPort = Integer.parseInt(scanner.nextLine());

      Peer peer = new Peer(serverPort, clientPort, name);
      peer.start();
    } catch (Exception e) {
      e.printStackTrace();
    };
  }
}
