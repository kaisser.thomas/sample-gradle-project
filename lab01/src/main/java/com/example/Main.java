package com.example;

import com.example.service.Calculator;
import com.example.ui.CalculatorUi;

public class Main {

  public static void main(String[] args) {
    Calculator calculator = new Calculator();
    CalculatorUi ui = new CalculatorUi(calculator);
    ui.start();
  }
}