package com.example.service;

public interface ICalculator {
  double add(double a, double b);
  double subtract(double a, double b);
  double multiply(double a, double b);
  double divide(double a, double b);
  double min(double a, double b);
  double max(double a, double b);
  double sqrt(double a);

}
