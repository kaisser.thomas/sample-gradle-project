package com.example.service;

public class Calculator implements ICalculator {

  private static final String SQUARE_ROOT_ERROR = "Square root of a negative number is not allowed.";
  private static final String DIVISION_BY_0_ERROR ="Division by zero is not allowed.";

  public double add(double a, double b) {
    return a + b;
  }

  public double subtract(double a, double b) {
    return a - b;
  }

  public double multiply(double a, double b) {
    return a * b;
  }

  public double divide(double a, double b) {
    if (b == 0) {
      throw new ArithmeticException(DIVISION_BY_0_ERROR);
    }
    return a / b;
  }

  public double min(double a, double b) {
    return a < b ? a : b;
  }

  public double max(double a, double b) {
    return a > b ? a : b;
  }

  public double sqrt(double a) {
    if (a < 0) {
      throw new ArithmeticException(SQUARE_ROOT_ERROR);
    }

    double epsilon = 1e-15;
    double estimate = a;

    while (abs(estimate - a / estimate) > epsilon * estimate) {
      estimate = (a / estimate + estimate) / 2.0;
    }

    return estimate;
  }

  public double abs(double a) {
    if (a < 0) {
      return -a;
    }
    return a;
  }

}
