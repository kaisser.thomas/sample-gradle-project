package com.example.ui;

import com.example.service.Calculator;
import java.util.Scanner;

public class CalculatorUi {
  private Calculator calculator;

  public CalculatorUi(Calculator calculator) {
    this.calculator = calculator;
  }

  public void start() {
    Scanner scanner = new Scanner(System.in);

    while (true) {
      System.out.println("Enter operation (+, -, *, /, min, max, sqrt) "
          + "or 'exit' to quit:");
      String operation = scanner.nextLine();

      if (operation.equalsIgnoreCase("exit")) {
        System.out.println("Goodbye!");
        break;
      }

      try {
        System.out.println("Enter first operand:");
        double operand1 = Double.parseDouble(scanner.nextLine());

        double result;
        if (operation.equals("sqrt")) {
          result = calculator.sqrt(operand1);
        } else {
          System.out.println("Enter second operand:");
          double operand2 = Double.parseDouble(scanner.nextLine());

          switch (operation) {
            case "+":
              result = calculator.add(operand1, operand2);
              break;
            case "-":
              result = calculator.subtract(operand1, operand2);
              break;
            case "*":
              result = calculator.multiply(operand1, operand2);
              break;
            case "/":
              result = calculator.divide(operand1, operand2);
              break;
            case "min":
              result = calculator.min(operand1, operand2);
              break;
            case "max":
              result = calculator.max(operand1, operand2);
              break;
            default:
              throw new IllegalArgumentException("Invalid operation.");
          }
        }

        System.out.println("Result: " + result);
      } catch (NumberFormatException e) {
        System.out.println("Invalid input. Please enter numeric operands.");
      } catch (ArithmeticException | IllegalArgumentException e) {
        System.out.println(e.getMessage());
      }
    }

    scanner.close();
  }
}
