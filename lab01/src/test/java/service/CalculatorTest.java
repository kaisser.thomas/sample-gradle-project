package service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.example.service.Calculator;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

  private final Calculator calculator;

  public CalculatorTest() {
    this.calculator = new Calculator();
  }

  @Test
  public void testAddition() {
    double result = calculator.add(2, 3);
    assertEquals(5, result, 0.0001);
  }

  @Test
  public void testSubtraction() {
    double result = calculator.subtract(5, 3);
    assertEquals(2, result, 0.0001);
  }

  @Test
  public void testMultiplication() {
    double result = calculator.multiply(2, 3);
    assertEquals(6, result, 0.0001);
  }

  @Test
  public void testDivision() {
    double result = calculator.divide(6, 3);
    assertEquals(2, result, 0.0001);
  }

  @Test
  public void testAbs() {
    double result = calculator.abs(-5);
    assertEquals(5, result, 0.0001);
  }

  @Test
  public void testMin() {
    double result = calculator.min(2, 3);
    assertEquals(2, result, 0.0001);
  }

  @Test
  public void testMax() {
    double result = calculator.max(2, 3);
    assertEquals(3, result, 0.0001);
  }

  @Test
  public void testSqrtValidData() {
    double result = calculator.sqrt(9);
    assertEquals(3, result, 0.0001);
  }

  @Test
  public void testSqrtInvalidData(){
    assertThrows(ArithmeticException.class,()->calculator.sqrt(-2));
  }

  @Test
  public void testDivisionInvalidData() {
    assertThrows(ArithmeticException.class,()->calculator.divide(3,0));
  }

}
