package service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.example.BigDecimalService;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;

public class BigDecimalServiceTest {
  @Test
  void testSum() {
    List<BigDecimal> list = List.of(new BigDecimal("1"), new BigDecimal("2"), new BigDecimal("3"));
    assertEquals(new BigDecimal("6"), BigDecimalService.sum(list));
  }

  @Test
  void testAverage() {
    List<BigDecimal> list = List.of(new BigDecimal("1"), new BigDecimal("2"), new BigDecimal("3"));
    assertEquals(new BigDecimal("2.0"), BigDecimalService.average(list));
  }

  @Test
  void testTop10Percent() {
    List<BigDecimal> list = List.of(
        new BigDecimal("10"), new BigDecimal("9"), new BigDecimal("8"),
        new BigDecimal("7"), new BigDecimal("6"), new BigDecimal("5"),
        new BigDecimal("4"), new BigDecimal("3"), new BigDecimal("2"),
        new BigDecimal("1")
    );
    List<BigDecimal> expected = List.of(new BigDecimal("10"));
    assertEquals(expected, BigDecimalService.top10Percent(list));
  }
}
