package com.example;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BigDecimalService {
  public static BigDecimal sum(List<BigDecimal> bigDecimalList) {
    return bigDecimalList.stream()
        .reduce(BigDecimal.ZERO, BigDecimal::add);
  }

  public static BigDecimal average(List<BigDecimal> bigDecimalList) {
    return BigDecimal.valueOf(bigDecimalList.stream()
        .mapToDouble(BigDecimal::doubleValue)
        .average()
        .orElse(BigDecimal.ZERO.doubleValue()));
  }

  public static List<BigDecimal> top10Percent(List<BigDecimal> bigDecimalList) {
    int count = (int) (bigDecimalList.size() * 0.1);
    return bigDecimalList.stream()
        .sorted(Comparator.reverseOrder())
        .limit(count)
        .collect(Collectors.toList());
  }

}
