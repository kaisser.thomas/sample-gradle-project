package com.example.benchmarks;


import com.example.model.Order;
import com.example.repository.HashSetBasedRepository;
import com.example.repository.InMemoryRepository;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class HashSetBasedRepositoryBenchmark {

  private InMemoryRepository<Order> repository;

  @Setup
  public void setup() {
    repository = new HashSetBasedRepository<>();
  }

  @Benchmark
  public void add() {
    repository.add(new Order(1, 100, 2));
  }

  @Benchmark
  public void contains() {
    repository.contains(new Order(1, 100, 2));
  }

  @Benchmark
  public void remove() {
    repository.remove(new Order(1, 100, 2));
  }

}
