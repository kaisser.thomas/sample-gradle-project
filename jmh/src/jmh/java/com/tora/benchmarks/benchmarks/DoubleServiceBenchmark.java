package com.tora.benchmarks.benchmarks;

import com.tora.benchmarks.service.DoubleService;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 3, time = 1)
@Measurement(iterations = 5, time = 1)
public class DoubleServiceBenchmark {
  private List<Double> doubleList;
  private double[] doubleArray;
  @Setup
  public void setup() {
    Random random = new Random();
    doubleList = new ArrayList<>();
    for (int i = 0; i < 100_000_000; i++) {
      doubleList.add(random.nextDouble());
    }

    doubleArray = new double[100_000_000];

    for (int i = 0; i < doubleArray.length; i++) {
      doubleArray[i] = random.nextDouble();
    }

  }

  @Benchmark
  public Double benchmarkSumDoubleObjects() {
    return DoubleService.sumDoubleObjects(doubleList);
  }

  @Benchmark
  public Double benchmarkAverageDoubleObjects() {
    return DoubleService.averageDoubleObjects(doubleList);
  }

  @Benchmark
  public List<Double> benchmarkTop10PercentDoubleObjects() {
    return DoubleService.top10PercentDoubleObjects(doubleList);
  }

  @Benchmark
  public double benchmarkSumDoubles() {
    return DoubleService.sumDoubles(doubleArray);
  }

  @Benchmark
  public double benchmarkAverageDoubles() {
    return DoubleService.averageDoubles(doubleArray);
  }

  @Benchmark
  public double[] benchmarkTop10PercentDoubles() {
    return DoubleService.top10PercentDoubles(doubleArray);
  }

}
