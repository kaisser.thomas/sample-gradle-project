package com.tora.benchmarks.service;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DoubleService {
  public static Double sumDoubleObjects(List<Double> doubleList) {
    return doubleList.stream()
        .reduce(0.0, Double::sum);
  }

  public static Double averageDoubleObjects(List<Double> doubleList) {
    return doubleList.stream()
        .mapToDouble(Double::doubleValue)
        .average()
        .orElse(0.0);
  }

  public static List<Double> top10PercentDoubleObjects(List<Double> doubleList) {
    int count = (int) (doubleList.size() * 0.1);
    return doubleList.stream()
        .sorted(Comparator.reverseOrder())
        .limit(count)
        .collect(Collectors.toList());
  }

  public static double sumDoubles(double[] array) {
    return Arrays.stream(array).sum();
  }

  public static double averageDoubles(double[] array) {
    return Arrays.stream(array).average().orElse(0.0);
  }

  public static double[] top10PercentDoubles(double[] array) {
    int count = (int) (array.length * 0.1);

    if (count >= array.length) {
      return Arrays.copyOf(array, array.length);
    } else {
      return Arrays.stream(array)
          .sorted()
          .skip(array.length - count)
          .toArray();


    }
  }

}
