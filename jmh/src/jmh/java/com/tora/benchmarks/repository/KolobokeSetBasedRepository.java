package com.tora.benchmarks.repository;

import com.koloboke.collect.set.ObjSet;
import com.tora.benchmarks.model.Entity;
import com.koloboke.collect.set.hash.HashObjSets;

public class KolobokeSetBasedRepository<ID,T extends Entity<ID>> implements InMemoryRepository<ID,T> {
  private final ObjSet<T> set;

  public KolobokeSetBasedRepository() {
    this.set = HashObjSets.newMutableSet();
  }

  @Override
  public void add(T t) {
    set.add(t);
  }

  @Override
  public boolean contains(T t) {
    return set.contains(t);
  }

  @Override
  public void remove(T t) {
    set.remove(t);
  }
}
