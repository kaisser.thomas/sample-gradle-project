package com.tora.benchmarks.repository;

import com.tora.benchmarks.model.Entity;
import com.tora.benchmarks.model.Order;
import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<ID, T extends Entity<ID>> implements InMemoryRepository<ID, T>{

  private final Set<T> set;

  public HashSetBasedRepository() {
    this.set = new HashSet<>();
  }

  @Override
  public void add(T t) {
    set.add(t);
  }

  @Override
  public boolean contains(T t) {
    return set.contains(t);
  }

  @Override
  public void remove(T t) {
    set.remove(t);
  }

}
