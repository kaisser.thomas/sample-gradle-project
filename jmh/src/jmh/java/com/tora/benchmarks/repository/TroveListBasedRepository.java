package com.tora.benchmarks.repository;

import com.tora.benchmarks.model.Entity;
import gnu.trove.list.array.TIntArrayList;

public class TroveListBasedRepository<ID, T extends Entity<Integer>> implements InMemoryRepository<Integer, T> {

  private final TIntArrayList orderIds;
  public TroveListBasedRepository() {
    this.orderIds = new TIntArrayList();
  }

  @Override
  public void add(T t) {
    orderIds.add(t.getId());
  }

  @Override
  public boolean contains(T t) {
    return orderIds.contains(t.getId());
  }

  @Override
  public void remove(T t) {
    orderIds.remove(t.getId());
  }

}
