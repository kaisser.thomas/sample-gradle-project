package com.tora.benchmarks.repository;

import com.tora.benchmarks.model.Entity;
import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<ID,T extends Entity<ID>> implements InMemoryRepository<ID,T> {
  private final List<T> list;

  public ArrayListBasedRepository() {
    this.list = new ArrayList<>();
  }

  @Override
  public void add(T t) {
    list.add(t);
  }

  @Override
  public boolean contains(T t) {
    return list.contains(t);
  }

  @Override
  public void remove(T t) {
    list.remove(t);
  }

}
