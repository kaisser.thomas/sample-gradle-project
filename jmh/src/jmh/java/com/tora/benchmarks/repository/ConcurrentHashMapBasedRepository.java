package com.tora.benchmarks.repository;

import com.tora.benchmarks.model.Entity;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<ID, T extends Entity<ID>> implements InMemoryRepository<ID, T> {

  private final ConcurrentHashMap<ID, T> map;

  public ConcurrentHashMapBasedRepository() {
    this.map = new ConcurrentHashMap<>();
  }

  @Override
  public void add(T t) {
    map.put(t.getId(),t);
  }

  @Override
  public boolean contains(T t) {
    return map.contains(t);
  }

  @Override
  public void remove(T t) {
    map.remove(t);
  }
}
