package com.tora.benchmarks.repository;

import com.tora.benchmarks.model.Entity;
import java.util.List;
import org.eclipse.collections.api.factory.Lists;

public class EclipseGSListBasedRepository<ID, T extends Entity<ID>> implements InMemoryRepository<ID,T> {

  private final List<T> list;

  public EclipseGSListBasedRepository() {
    this.list = Lists.mutable.empty();
  }

  @Override
  public void add(T t) {
    list.add(t);
  }

  @Override
  public boolean contains(T t) {
    return list.contains(t);
  }

  @Override
  public void remove(T t) {
    list.remove(t);
  }
}
