package com.tora.benchmarks.repository;

import com.tora.benchmarks.model.Entity;

public interface InMemoryRepository<ID, T extends Entity<ID>> {

  void add(T t);

  boolean contains(T t);

  void remove(T t);

}
