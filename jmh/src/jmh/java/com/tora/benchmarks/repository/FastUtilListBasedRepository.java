package com.tora.benchmarks.repository;

import com.tora.benchmarks.model.Entity;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class FastUtilListBasedRepository<ID, T extends Entity<ID>> implements InMemoryRepository<ID, T>  {
  private final ObjectArrayList<T> list;

  public FastUtilListBasedRepository() {
    this.list = new ObjectArrayList<>();
  }

  @Override
  public void add(T t) {
    list.add(t);
  }

  @Override
  public boolean contains(T t) {
    return list.contains(t);
  }

  @Override
  public void remove(T t) {
    list.remove(t);
  }
}
